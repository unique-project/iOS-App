//
//  UQIDApi.swift
//  Unique
//
//  Created by Alex on 22/06/16.
//  Copyright © 2016 Alex Steiner. All rights reserved.
//

import Foundation
import Alamofire

class UQIDApi {
    private let baseURL = "https://unique.alexsteiner.de"
    private let apiKey = "CRSGS>Jo)u@UBwYJ"
    
    private static var sharedManager:Manager?
    
    init() {
        let serverTrustPolicy = ServerTrustPolicy.PinCertificates(
            certificates: ServerTrustPolicy.certificatesInBundle(CoreDataManager.frameworkBundle()),
            validateCertificateChain: true,
            validateHost: true
        )

        let serverTrustPolicies:[String: ServerTrustPolicy] = [
            "unique.alexsteiner.de": serverTrustPolicy
        ]
        
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        configuration.requestCachePolicy = .ReloadIgnoringLocalCacheData
        
        UQIDApi.sharedManager = Manager(
            configuration: configuration,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies))
    }
    
    func send(id id: UQID, completion:(successfull: Bool, unique: Bool?, uniqueIndex: Int?)->Void) {
        UQIDApi.sharedManager?.request(.POST, baseURL + "/add", parameters: ["uuid": id.UUID, "puuid": id.PUUID, "deviceId": UQDevice.deviceId], encoding: ParameterEncoding.JSON, headers: ["APIKEY": apiKey]).responseJSON() {
            response in
            if let json = response.result.value as? Dictionary<String, AnyObject> where response.response?.statusCode == 201 {
                completion(successfull: true, unique: json["unique"] as? Bool, uniqueIndex: json["uniqueIndex"] as? Int)
            }
            else {
                completion(successfull: false, unique: nil, uniqueIndex: nil)
            }
        }
    }
}