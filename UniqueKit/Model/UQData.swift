//
//  UQData.swift
//  Unique
//
//  Created by Alex on 23/06/16.
//  Copyright © 2016 Alex Steiner. All rights reserved.
//

import Foundation
import CryptoSwift

public class UQData {
    
    public let type: UQDataType
    public let hash: String
    
    init(type: UQDataType, value: AnyObject) {
        self.type = type
        self.hash = "\(type.rawValue):\(value)".sha512()
    }
    
}

public enum UQDataType : String {
    case DeviceName
    case DeviceModel
    case DeviceSystem
    case MutlitaskingSupported
    case Supports3DTouch
    case BoldText
    case GrayScale
    case MonoAudio
    case ShakeToUndo
    case SpeakScreen
    case GuidedAccess
    case InvertColors
    case SwitchControl
    case ReduceTransparency
    case ReduceMotion
    case CarrierName
    case CarrierAllowsVOIP
    case LanguageCode
    case CountryCode
    case Keyboards
    case IAPAllowed
    case FacebookSetup
    case TwitterSetup
    
    case AlbumTitles
    case CalendarTitles
    case ReminderTitles
    
    public var length: Int {
        switch self {
        case .DeviceName:
            return 8
        default:
            return 4
        }
    }
    
    public init(index: Int) {
        let all:Array<UQDataType> = [.AlbumTitles, .CalendarTitles, .ReminderTitles, .DeviceName, .DeviceModel, .DeviceSystem, .MutlitaskingSupported, .Supports3DTouch, .BoldText, .GrayScale, .MonoAudio, .ShakeToUndo, .SpeakScreen, .GuidedAccess, .InvertColors, .SwitchControl, .ReduceTransparency, .ReduceMotion, .CarrierName, .CarrierAllowsVOIP, .LanguageCode, .CountryCode, .Keyboards, .IAPAllowed, .FacebookSetup, .TwitterSetup]
        self = all[index]
        
    }
}
