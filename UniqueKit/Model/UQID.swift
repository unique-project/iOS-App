//
//  UQID.swift
//  Unique
//
//  Created by Alex on 20/06/16.
//  Copyright © 2016 Alex Steiner. All rights reserved.
//

import Foundation
import CryptoSwift
import CryptoKit

public class UQID {
    
    public var UUID = String()
    public var PUUID = String()
    public let device: UQDevice
    
    init(generateByDevice device: UQDevice) {
        self.device = device
        let hash = generatePublicID(device.publicData)
        self.UUID = hash
        self.PUUID = hash
    }
    
    init() {
        device = UQDevice()
        let hash = generatePublicID(device.publicData)
        self.UUID = hash
        self.PUUID = hash
    }
    
    private func generatePublicID(publicData: Array<UQData>)->String {
        var value = String()
        for data in publicData {
            value += UQID.hash(data.hash, length: data.type.length)
        }
        
        return value
    }
    
    public class func generateId(completion: (id: UQID)->Void) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            let id = UQID()
            completion(id: id)
        })
    }
    
    class func hash(password: String, length:Int)->String {
        let salt = "YEcuFvfWCA9PsYpoJ3k6mUz4cgKl9Kc3ajNRlwxvwTGb9nX/GPsxdx+740C6hwK8OqC9nz6Is7/MRgoG9bWUDRM5ZnHCYdOXEA0iX05OQte8Nnc7+2ohdu0jEnSJOMTVqbXPoz/gKSfmojEZ3YedhKkYEQnrvXgl1vk8eidnk0zpPVcHvopwlbbyRUFgEGW1IerhDLt7HKOGmROToWO1P1UaAiyJXW//3J7ykDbAcFKsyhit1Eatdey5fX58UcEVmwYW5xztcqpm4QqMnFOixyFzCylAU5zbPonE6c48Xxiy+agPfwucGHLa+gA1KEM0hp69uXiWbf1WfDfystxRBg=="
        let pseudoAlgo:Int32 = 1
        let keyLength:Int32 = Int32(length)
        let rounds:Int32 = 64000
        
        let value = ASCrypto.generate(password, withSalt: salt, pseudoAlgo: pseudoAlgo, keyLength: keyLength, rounds: rounds)
        return value
    }
    
    public func sendToServer(completion:(successfull: Bool, unique: Bool?, uniqueIndex: Int?)->Void) {
        UQIDApi().send(id: self, completion: {
            successfull, unique, uniqueIndex in
            completion(successfull: successfull, unique: unique, uniqueIndex: uniqueIndex)
            if let unique = unique where successfull {
                print("ID successfully sent")
                if unique {
                    print("The ID is unique")
                }
                else {
                    print("The ID is not unique!")
                }
            }
            else {
                print("Sending failed!")
            }
        })
    }
    
    public func fetchPrivateID(completion:(successful: Bool)->Void) {
        self.device.fetchPrivateData() {
            device in
            var PUUID = String()
            for data in device.privateData {
                PUUID = PUUID + UQID.hash(data.hash, length: 16)
            }
            self.PUUID = PUUID + self.UUID
            completion(successful: true)
        }
    }
}