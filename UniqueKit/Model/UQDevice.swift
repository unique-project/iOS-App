//
//  UQDevice.swift
//  Unique
//
//  Created by Alex on 20/06/16.
//  Copyright © 2016 Alex Steiner. All rights reserved.
//

import UIKit
import CoreTelephony
import Foundation
import StoreKit
import Social
import SystemConfiguration.CaptiveNetwork
import Photos
import EventKit

public class UQDevice {
    
    public var publicData = Array<UQData>()
    public var privateData = Array<UQData>()
    
    init() {
        let currentDevice = UIDevice.currentDevice()
        
        publicData.append(UQData(type: .DeviceName, value: currentDevice.name))
        publicData.append(UQData(type: .DeviceModel, value: currentDevice.model))
        publicData.append(UQData(type: .DeviceSystem, value: currentDevice.systemVersion))
        publicData.append(UQData(type: .MutlitaskingSupported, value: currentDevice.multitaskingSupported))
        publicData.append(UQData(type: .Supports3DTouch, value: UITraitCollection().forceTouchCapability.rawValue))
        publicData.append(UQData(type: .BoldText, value: UIAccessibilityIsBoldTextEnabled()))
        publicData.append(UQData(type: .GrayScale, value: UIAccessibilityIsGrayscaleEnabled()))
        publicData.append(UQData(type: .MonoAudio, value: UIAccessibilityIsMonoAudioEnabled()))
        publicData.append(UQData(type: .ShakeToUndo, value: UIAccessibilityIsShakeToUndoEnabled()))
        publicData.append(UQData(type: .SpeakScreen, value: UIAccessibilityIsSpeakScreenEnabled()))
        publicData.append(UQData(type: .GuidedAccess, value: UIAccessibilityIsGuidedAccessEnabled()))
        publicData.append(UQData(type: .InvertColors, value: UIAccessibilityIsInvertColorsEnabled()))
        publicData.append(UQData(type: .SwitchControl, value: UIAccessibilityIsSwitchControlRunning()))
        publicData.append(UQData(type: .ReduceTransparency, value: UIAccessibilityIsReduceTransparencyEnabled()))
        publicData.append(UQData(type: .ReduceMotion, value: UIAccessibilityIsReduceMotionEnabled()))
        
        let carrier = CTCarrier()
        if let carrierName = carrier.carrierName {
            publicData.append(UQData(type: .CarrierName, value: carrierName))
        }
        publicData.append(UQData(type: .CarrierAllowsVOIP, value: carrier.allowsVOIP))
        
        let langId = NSLocale.currentLocale().objectForKey(NSLocaleLanguageCode) as! String
        let countryId = NSLocale.currentLocale().objectForKey(NSLocaleCountryCode) as! String
        publicData.append(UQData(type: .LanguageCode, value: langId))
        publicData.append(UQData(type: .CountryCode, value: countryId))
        
        if let installedKeyboards = NSUserDefaults.standardUserDefaults().objectForKey("AppleKeyboards") {
            publicData.append(UQData(type: .Keyboards, value: installedKeyboards))
        }
        
        publicData.append(UQData(type: .IAPAllowed, value: SKPaymentQueue.canMakePayments()))
        
        publicData.append(UQData(type: .FacebookSetup, value: SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook)))
        publicData.append(UQData(type: .TwitterSetup, value: SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter)))
    }
    
    public func fetchPrivateData(completion:(device: UQDevice)->Void) {
        fetchPrivateData(0, completion: completion)
    }
    
    private func fetchPrivateData(type: Int, completion:(device: UQDevice)->Void) {
        dispatch_async(dispatch_get_main_queue(), {
            switch type {
            case 0:
                self.fetchPhotoAlbums() {
                    _ in
                    self.fetchPrivateData(type+1, completion: completion)
                }
            case 1:
                self.fetchCalendars() {
                    _ in
                    self.fetchPrivateData(type+1, completion: completion)
                }
            case 2:
                self.fetchReminders() {
                    _ in
                    self.fetchPrivateData(type+1, completion: completion)
                }
            default:
                completion(device: self)
            }
        })
    }
    
    private func fetchPhotoAlbums(completion:(successful: Bool)->Void) {
        switch PHPhotoLibrary.authorizationStatus() {
        case .Authorized:
            self.privateData.append(UQData(type: .AlbumTitles, value: getPhotoAlbums()))
            completion(successful: true)
        case .NotDetermined:
            PHPhotoLibrary.requestAuthorization() {
                status in
                self.privateData.append(UQData(type: .AlbumTitles, value: self.getPhotoAlbums()))
                completion(successful: status == .Authorized)
            }
        default:
            completion(successful: false)
        }
    }
    
    private func fetchCalendars(compl:(successful: Bool)->Void) {
        let eventStore = EKEventStore()
        switch EKEventStore.authorizationStatusForEntityType(.Event) {
        case .Authorized:
            self.privateData.append(UQData(type: .CalendarTitles, value: getEventTypes(ofType: .Event)))
            compl(successful: true)
        case .NotDetermined:
            eventStore.requestAccessToEntityType(.Event, completion: {
                suc, error in
                if suc {
                    self.privateData.append(UQData(type: .CalendarTitles, value: self.getEventTypes(ofType: .Event)))
                }
                compl(successful: suc)
            })
        default:
            compl(successful: false)
        }
    }
    
    private func fetchReminders(compl:(successful: Bool)->Void) {
        let eventStore = EKEventStore()
        switch EKEventStore.authorizationStatusForEntityType(.Reminder) {
        case .Authorized:
            self.privateData.append(UQData(type: .ReminderTitles, value: getEventTypes(ofType: .Reminder)))
            compl(successful: true)
        case .NotDetermined:
            eventStore.requestAccessToEntityType(.Reminder, completion: {
                suc, error in
                if suc {
                    self.privateData.append(UQData(type: .ReminderTitles, value: self.getEventTypes(ofType: .Reminder)))
                }
                compl(successful: suc)
            })
        default:
            compl(successful: false)
        }

    }
    
    private func getEventTypes(ofType type: EKEntityType)->String {
        let eventStore = EKEventStore()
        var dataString = String()
        let allEntities = eventStore.calendarsForEntityType(type)
        for entity in allEntities {
            dataString = dataString+entity.title
        }
        return dataString
    }
    
    private func getPhotoAlbums()->String {
        let fetchOptions = PHFetchOptions()
        let smartAlbums = PHAssetCollection.fetchAssetCollectionsWithType(.SmartAlbum, subtype: .Any, options: fetchOptions)
        let topLevelUserCollections = PHCollectionList.fetchTopLevelUserCollectionsWithOptions(fetchOptions)
        var albums = String()
        for allAlbums in [topLevelUserCollections, smartAlbums] {
            allAlbums.enumerateObjectsUsingBlock { (asset, index, stop) -> Void in
                if let a = asset as? PHAssetCollection, title = a.localizedTitle {
                    albums = albums+title
                }
            }
        }
        return albums
    }
    
    static var deviceId:String {
        let userDefault = NSUserDefaults.standardUserDefaults()
        if let id = userDefault.valueForKey("DEVICE-ID") as? String {
            return id
        }
        else {
            let id = NSUUID().UUIDString
            userDefault.setValue(id, forKey: "DEVICE-ID")
            userDefault.synchronize()
            return id
        }
    }
    
    public class func userState()->Int {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        return max(userDefaults.integerForKey("USER-STATE"), 1)
    }
    
    public class func setUserState(state: Int) {
        guard state > userState() else { return }
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setInteger(state, forKey: "USER-STATE")
        userDefaults.synchronize()
    }
}