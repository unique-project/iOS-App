//
//  ASCrypto.h
//  Alex Steiner
//
//  Created by Alex on 16/04/16.
//  Copyright © 2016 Alex Steiner. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ASCrypto : NSObject

+(NSString *)generate:(NSString *)password withSalt:(NSString *)salt pseudoAlgo:(int)hashAlg keyLength:(int)keyLength rounds:(int)rounds;
+(NSString *)randomSalt:(int)saltLength;

@end
