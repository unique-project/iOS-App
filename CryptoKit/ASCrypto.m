//
//  ASCrypto.m
//  Alex Steiner
//
//  Created by Alex on 16/04/16.
//  Copyright © 2016 Alex Steiner. All rights reserved.
//

#import "ASCrypto.h"
#import <CommonCrypto/CommonKeyDerivation.h>
#import <CommonCrypto/CommonCrypto.h>
#include <stdlib.h>

@implementation ASCrypto

+(NSString *)generate:(NSString *)password withSalt:(NSString *)salt pseudoAlgo:(int)hashAlg keyLength:(int)keyLength rounds:(int)rounds {
    NSMutableData *key = [NSMutableData dataWithLength:keyLength];
    NSData *saltData = [[NSData alloc] initWithBase64EncodedString:salt options:0];

    CCKeyDerivationPBKDF(kCCPBKDF2, password.UTF8String, password.length, saltData.bytes, saltData.length, hashAlg, rounds, key.mutableBytes, key.length);
    
    NSString* skey = [key base64EncodedStringWithOptions:0];
    return skey;
}

+(NSString *)randomSalt:(int)saltLength {
    NSMutableData* theData = [NSMutableData dataWithCapacity:saltLength];
    for( unsigned int i = 0 ; i < saltLength/4 ; ++i )
    {
        u_int32_t randomBits = arc4random();
        [theData appendBytes:(void*)&randomBits length:4];
    }
    return [theData base64EncodedStringWithOptions:0];
}

@end
