//
//  CryptoKit.h
//  CryptoKit
//
//  Created by Alex on 27/06/16.
//  Copyright © 2016 Alex Steiner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CryptoKit/ASCrypto.h>

//! Project version number for CryptoKit.
FOUNDATION_EXPORT double CryptoKitVersionNumber;

//! Project version string for CryptoKit.
FOUNDATION_EXPORT const unsigned char CryptoKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CryptoKit/PublicHeader.h>


