//
//  AboutViewController.swift
//  Unique
//
//  Created by Alex on 11/07/16.
//  Copyright © 2016 Alex Steiner. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {

    //MARK: Actions
    @IBAction private func twitter(sender: UIButton) {
        if let url = NSURL(string: "twitter://user?screen_name=alexsteinerde") {
            UIApplication.sharedApplication().openURL(url)
        }
    }
    
    @IBAction private func rebtsoft(sender: UITapGestureRecognizer) {
        if let url = NSURL(string: "http://www.rebtsoft.com") {
            UIApplication.sharedApplication().openURL(url)
        }
    }
    //MARK: Outlets
    
    
    //MARK: Attributes
    
    
    //MARK: Main
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
