//
//  InformationTableViewController.swift
//  Unique
//
//  Created by Alex on 05/07/16.
//  Copyright © 2016 Alex Steiner. All rights reserved.
//

import UIKit
import SafariServices
import UniqueKit

class InformationTableViewController: UITableViewController {

    //MARK: Actions
    @IBAction private func done(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK: Outlets
    
    
    //MARK: Attributes
    
    
    //MARK: Main
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 0 {
            let controller = SFSafariViewController(URL: NSURL(string: "http://alexsteiner.de/uniqueid/licenses.html")!)
            self.presentViewController(controller, animated: true, completion: nil)
        }
        tableView.cellForRowAtIndexPath(indexPath)?.selected = false
    }

}
