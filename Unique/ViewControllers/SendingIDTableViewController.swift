//
//  SendingIDTableViewController.swift
//  Unique
//
//  Created by Alex on 13/07/16.
//  Copyright © 2016 Alex Steiner. All rights reserved.
//

import UIKit
import UniqueKit

class SendingIDTableViewController: DynamicCellsTableViewController {

    //MARK: Actions
    @IBAction private func done() {
        self.navigationController?.popToRootViewControllerAnimated(true)
        UQDevice.setUserState(3)
    }
    
    //MARK: Outlets
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var imageView: UIImageView!
    
    
    //MARK: Attributes
    var id: UQID?
    var idSent = false
    //MARK: Main
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.backgroundColor = UIColor(red: 0, green: (118/255), blue: (184/255), alpha: 0.7)
        self.refreshControl?.tintColor = UIColor.whiteColor()
        self.tableView.addSubview(refreshControl!)
        
        self.refreshControl?.beginRefreshing()
        id?.sendToServer() {
            successfull, unique, uniqueIndex in
            if let unique = unique where successfull {
                AppDelegate.registerRepeatNotification()
                
                let state = self.stateUI(unique, uniqueIndex: uniqueIndex)
                self.titleLabel.text = state.title
                self.descriptionLabel.text = state.description
                self.imageView.image = state.image
            }
            else {
                self.imageView.image = nil
                self.titleLabel.text = NSLocalizedString("Error", comment: "State text")
                self.descriptionLabel.text = NSLocalizedString("An error during the sending process occurred", comment: "State text")
            }
            self.idSent = true
            self.tableView.beginUpdates()
            self.refreshControl?.endRefreshing()
            self.refreshControl = nil
            self.tableView.insertRowsAtIndexPaths([NSIndexPath(forRow: 0, inSection: 0)], withRowAnimation: .Automatic)
            self.tableView.endUpdates()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func stateUI(unique: Bool, uniqueIndex: Int?)->(title: String, description: String, image: UIImage?) {
        if let uniqueIndex = uniqueIndex where unique {
            let type = UQDataType(index: uniqueIndex)
            return (NSLocalizedString("Unique", comment: "State text"),
                    String.localizedStringWithFormat(NSLocalizedString("Your fingerprint is unique and only you have the data on your device.\nFollowing data makes you unique: %@", comment: "State text"), DataListViewController.titleOfData(type)),
            UIImage(named: "IdVerified"))
        }
        else {
            return (NSLocalizedString("Not Unique", comment: "State text"),
                    NSLocalizedString("Your data is not unique and some other devices share the same data as you.", comment: "State text"),
                    UIImage(named: "IdNotVerified"))
        }
    }
}

extension SendingIDTableViewController {
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return idSent ? 1 : 0
    }
}
