//
//  RepeatTableViewController.swift
//  Unique
//
//  Created by Alex on 05/07/16.
//  Copyright © 2016 Alex Steiner. All rights reserved.
//

import UIKit

class RepeatTableViewController: DynamicCellsTableViewController {

    //MARK: Actions
    @IBAction private func done(sender: UIBarButtonItem) {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    //MARK: Outlets
    @IBOutlet private weak var doneButton: UIBarButtonItem!
    
    //MARK: Attributes
    
    
    //MARK: Main
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // Activate the notifications
        let action = UIMutableUserNotificationAction()
        action.identifier = "FINGERPRINT"
        action.title = NSLocalizedString("Fingerprint", comment: "Notification action button title")
        action.activationMode = .Foreground
        action.destructive = false
        
        let category = UIMutableUserNotificationCategory()
        category.identifier = "FINGERPRINT"
        category.setActions([action], forContext: .Default)
        
        let notificationSettings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: [category])
        UIApplication.sharedApplication().registerUserNotificationSettings(notificationSettings)
        
        AppDelegate.registerRepeatNotification()
        
        tableView.cellForRowAtIndexPath(indexPath)?.selected = false
        doneButton.enabled = true
    }

}
