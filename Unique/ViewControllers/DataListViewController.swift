//
//  DataListViewController.swift
//  Unique
//
//  Created by Alex on 25/06/16.
//  Copyright © 2016 Alex Steiner. All rights reserved.
//

import UIKit
import UniqueKit

class DataListViewController: UITableViewController {

    //MARK: Actions
    @IBAction private func send() {
        if let id = id {
            self.performSegueWithIdentifier("send", sender: id)
        }
    }

    //MARK: Outlets
    
    //MARK: Attributes
    var id: UQID? {
        didSet {
            dispatch_async(dispatch_get_main_queue(), {
                self.navigationItem.rightBarButtonItem?.enabled = true
                self.tableView.reloadData()
                self.refreshControl = nil
            })
        }
    }
    
    //MARK: Main
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        guard id == nil else { return }
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.backgroundColor = UIColor(red: 0, green: (118/255), blue: (184/255), alpha: 0.7)
        self.refreshControl?.tintColor = UIColor.whiteColor()
        self.tableView.addSubview(refreshControl!)
        
        self.refreshControl?.beginRefreshing()
        UQID.generateId() {
            id in
            id.fetchPrivateID() {
                _ in
                print("ID fetched: \(id.PUUID)")
                self.refreshControl?.endRefreshing()
                self.id = id
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let controller = segue.destinationViewController as? SendingIDTableViewController, id = sender as? UQID {
            controller.id = id
        }
    }

    class func titleOfData(type: UQDataType)->String {
        switch type {
        case .DeviceName:
            return NSLocalizedString("Device name", comment: "Data type title")
        case .DeviceModel:
            return NSLocalizedString("Device model", comment: "Data type title")
        case .DeviceSystem:
            return NSLocalizedString("Device system", comment: "Data type title")
        case .MutlitaskingSupported:
            return NSLocalizedString("Multitasking supported", comment: "Data type title")
        case .Supports3DTouch:
            return NSLocalizedString("Device supports 3D-Touch", comment: "Data type title")
        case .BoldText:
            return NSLocalizedString("Bold text enabled", comment: "Data type title")
        case .GrayScale:
            return NSLocalizedString("Gray scale enabled", comment: "Data type title")
        case .MonoAudio:
            return NSLocalizedString("Mono audio enabled", comment: "Data type title")
        case .ShakeToUndo:
            return NSLocalizedString("Shake to undo enabled", comment: "Data type title")
        case .SpeakScreen:
            return NSLocalizedString("Speak screen enabled", comment: "Data type title")
        case .GuidedAccess:
            return NSLocalizedString("Guided access enabled", comment: "Data type title")
        case .InvertColors:
            return NSLocalizedString("Invert colors enabled", comment: "Data type title")
        case .SwitchControl:
            return NSLocalizedString("Switch control enabled", comment: "Data type title")
        case .ReduceTransparency:
            return NSLocalizedString("Reduce transparency enabled", comment: "Data type title")
        case .ReduceMotion:
            return NSLocalizedString("Reduce motion enabled", comment: "Data type title")
        case .CarrierName:
            return NSLocalizedString("Carrier name", comment: "Data type title")
        case .CarrierAllowsVOIP:
            return NSLocalizedString("Carrier allows VOIP", comment: "Data type title")
        case .LanguageCode:
            return NSLocalizedString("Language", comment: "Data type title")
        case .CountryCode:
            return NSLocalizedString("Region", comment: "Data type title")
        case .Keyboards:
            return NSLocalizedString("Keyboards", comment: "Data type title")
        case .IAPAllowed:
            return NSLocalizedString("In-App purchases allowed", comment: "Data type title")
        case .FacebookSetup:
            return NSLocalizedString("Facebook setup", comment: "Data type title")
        case .TwitterSetup:
            return NSLocalizedString("Twitter setup", comment: "Data type title")
        case .AlbumTitles:
            return NSLocalizedString("Album titles", comment: "Data type title")
        case .CalendarTitles:
            return NSLocalizedString("Calendar titles", comment: "Data type title")
        case .ReminderTitles:
            return NSLocalizedString("Reminder list titles", comment: "Data type title")
        }
    }
    
}

extension DataListViewController {
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return id == nil ? 0 : 2
        case 1:
            return id?.device.publicData.count ?? 0
        default:
            return id?.device.privateData.count ?? 0
        }
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return [NSLocalizedString("Your IDs", comment: "Data list header title"),
                NSLocalizedString("Public data", comment: "Data list header title"),
                NSLocalizedString("Private data", comment: "Data list header title")][section]
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .Subtitle, reuseIdentifier: nil)
        cell.selectionStyle = .None
        if let id = id where indexPath.section == 0 {
            cell.textLabel?.text = [NSLocalizedString("Public ID", comment: "Data list title"), NSLocalizedString("Private ID", comment: "Data list title")][indexPath.row]
            cell.detailTextLabel?.text = [id.UUID, id.PUUID][indexPath.row]
        }
        else if let id = id where indexPath.section == 1 {
            let data = id.device.publicData[indexPath.row]
            cell.textLabel?.text = DataListViewController.titleOfData(data.type)
            cell.detailTextLabel?.text = data.hash
        }
        else if let id = id where indexPath.section == 2 {
            let data = id.device.privateData[indexPath.row]
            cell.textLabel?.text = DataListViewController.titleOfData(data.type)
            cell.detailTextLabel?.text = data.hash
        }
        return cell
    }
}
