//
//  HomeTableViewController.swift
//  Unique
//
//  Created by Alex on 03/07/16.
//  Copyright © 2016 Alex Steiner. All rights reserved.
//

import UIKit
import UniqueKit

class HomeTableViewController: UITableViewController {

    //MARK: Actions
    @IBAction private func goToInformation() {
        
    }
    
    @IBAction private func contact() {
        guard let url = NSURL(string: "mailto:info@alexsteiner.de") else { return }
        UIApplication.sharedApplication().openURL(url)
    }
    
    //MARK: Outlets
    
    
    //MARK: Attributes
    private var rowNumber = 0
    
    //MARK: Main
    override func viewDidLoad() {
        super.viewDidLoad()
        rowNumber = UQDevice.userState()
        tableView.estimatedRowHeight = 94
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        let rowDifferent = UQDevice.userState() - rowNumber
        if rowDifferent > 0 {
            tableView.beginUpdates()
            var indexPaths = Array<NSIndexPath>()
            
            for index in rowNumber...rowNumber+rowDifferent-1 {
                indexPaths.append(NSIndexPath(forRow: index, inSection: 0))
            }
            tableView.insertRowsAtIndexPaths(indexPaths, withRowAnimation: .Automatic)
            rowNumber = UQDevice.userState()
            tableView.endUpdates()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension HomeTableViewController {
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowNumber
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.row {
        case 0:
        //TODO: Add going to the info view
            break
        case 1:
        //TODO: Add going to data list
            break
        case 2:
        //TODO: Add goint to repeat view
            break
        default:
            break
        }
    }
}
