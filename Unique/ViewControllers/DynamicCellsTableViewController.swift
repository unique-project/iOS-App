//
//  DynamicCellsTableViewController.swift
//  Unique
//
//  Created by Alex on 03/07/16.
//  Copyright © 2016 Alex Steiner. All rights reserved.
//

import UIKit

class DynamicCellsTableViewController: UITableViewController {

    //MARK: Actions
    
    
    //MARK: Outlets
    
    
    //MARK: Attributes
    
    
    //MARK: Main
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 94
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

}
