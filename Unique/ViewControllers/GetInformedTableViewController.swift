//
//  GetInformedTableViewController.swift
//  Unique
//
//  Created by Alex on 12/07/16.
//  Copyright © 2016 Alex Steiner. All rights reserved.
//

import UIKit
import UniqueKit

class GetInformedTableViewController: DynamicCellsTableViewController {

    //MARK: Actions
    
    
    //MARK: Outlets
    
    
    //MARK: Attributes
    
    
    //MARK: Main
    override func viewDidLoad() {
        super.viewDidLoad()
        UQDevice.setUserState(2)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
