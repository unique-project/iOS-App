# Texts

## Get informed
### The Project
- Allgemeine Informationen zu dem Projekt, und wieso es gemacht wird.

This app is a school project which should find out if a iOS device is identifiable with only the access to the data. Apple gives no access to a unique identifier which is shared between multiple apps.
With this app you have the chance to check if you and your data are unique or just one in the mass. 

Diese App ist Teil eines Schulprojektes, in dem untersucht wird, ob ein IOS Gerät eindeutig identifizeribar ist, wenn man nur Zugriff auf die Daten hat. Apple gibt Entwicklern nämlich keine Möglichkeit auf eine eindeutige ID zuzugreifen, die auch App-übergreifend vorhanden ist. Mit dieser App hast du die Möglichkeit zu schauen, ob du mit deinen Daten einzigartig bist, oder doch nur einer unter vielen.

### Fingerprinting
- Wozu es gut sein könnte
- Was genau passiert, und dass es sicher ist

The generated identifier reflects a fingerprint of your device and can be used for example to show you personalized advertisements. But do not be worried, the app will not collect any data from you and runs under the highest standarts of security. Finally the collected data cannot even lead back to you or any of your personal data because only a hash will be stored!
If you have any questions regarding this project, please feel free to contact me via the contact button on the home page.

Die generierte ID spiegelt quasi einen Fingerabdruck von deinem Gerät wider und kann zum Beispiel für personalisierte Werbung genutzt werden. Aber keine Sorge, diese App möchte nicht deine Daten sammeln und arbeitet mit sehr hohen Sicherheitsmechanismen. Letztendlich können die gesammelten Daten nicht auf dich und deinen persönlichen Daten zurück führen, denn es wird nur ein Hash gespeichert. 
Wenn du irgendwelche Fragen hast, kannst du mich gerne über den Kontakt-Button auf der Startseite kontaktieren.

## Collect and send
### Collect
- Was beim Sammeln passiert
- Auf Berechtigungen hinweisen

During the collection the app will need the permissions to access your photos, calendars and reminders. Do not be worried, all collected data is hashed anonymously and cannot be transferred back into the origin data!

Während des Sammelns wirst du nach Berechtigungen für Fotos, Kalender und Erinnerungen gefragt. Keine Angst, diese Daten werden alle gehasht, unkentlich gemacht und sind somit nicht nachverfolgbar.
### Send
- Was beim Senden passiert, wo es hingeht
- Was man zurück bekommt

After collecting all the possible data you can send your IDs to evaluate them. Only the final two IDs will be sent to me so it is completely anonymously. With the sending you can check if your fingerprint of the device is unique.

Nachdem erfolgreichen Generieren der IDs kannst du diese zum Auswerten an mich senden. Die finalen zwei IDs werden damit komplett anonym mit mir geteilt und du erfährst, ob diese einzigartig sind. Dies hilft vor allem auch mir bei der Auswertung meiner Untersuchung.
## Repeat
### Repeat
To check if your device data is still unique please activate notifications to repeat the collection a few times. This also supports the accurateness of the analysed data

Um zu prüfen ob dein Gerät auch nach einiger Zeit noch einzigartig ist, empfiehlt es sich den Prozess einige Male zu wiederholen. Das unterstützt ebenfalls die Genauigkeit der Daten und hilft vor allem mir bei meiner Untersuchung.