//
//  UniqueKitTests.swift
//  UniqueKitTests
//
//  Created by Alex on 20/06/16.
//  Copyright © 2016 Alex Steiner. All rights reserved.
//

import XCTest
@testable import UniqueKit
import CryptoKit

class UniqueKitTests: XCTestCase {
    
    
    func testId() {
        var uuids = Array<String>()
        let device = UQDevice()
        self.measureBlock({
            let id = UQID(generateByDevice: device).UUID
            if !uuids.contains(id) {
                uuids.append(id)
            }
        })
        XCTAssert(uuids.count == 1)
    }
    
    func testDevice() {
        var uuids = Array<String>()
        self.measureBlock({
            let device = UQDevice()
            let id = device.publicHash
            if !uuids.contains(id) {
                uuids.append(id)
            }
        })
        XCTAssert(uuids.count == 1)
    }
    
    func testSha512() {
        self.measureBlock({
            UQData(type: .BoldText, value: true).hash
        })
    }
    
    func testSalt() {
        print(ASCrypto.randomSalt(256))
    }
    
    func testInstalledKeyboards() {
        print(NSUserDefaults.standardUserDefaults().objectForKey("AppleKeyboards"))
    }
    
}
